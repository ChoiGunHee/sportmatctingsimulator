package com.dku.mobilelab.sportmatcing.recordcreator;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.database.SportsDao;
import com.dku.mobilelab.sportmatcing.vo.Sports;

public class SportsRecordsCreator {

	ArrayList<Sports> sportsList;
	
	/* Match 테이블에 따라 업데이트됨. */
	public SportsRecordsCreator(int memberID, String teamID){
		
		TeamRecordsCreator teamRecords = null;
		double loyality;
		int teamID_index=0; // 수정
		
		sportsList = teamRecords.sportsList; //갱신해야할 리스트

		while(!teamRecords.teamList.get(teamID_index).getTeamID().equals(teamID)) //TRUE일 때
			teamID_index++;
	      
		/* 갱신 위해 종목 정보 레코드 찾기 */
		for(int i=0; i<sportsList.size(); i++){
			
			if(sportsList.get(i).getSportsID().equals(sportsID(memberID))
					&& sportsList.get(i).getTeamID().equals(teamID)){
				
				int member_gameNumber;
				int team_gameNumber = 0;
				DecimalFormat doubleFormat = new DecimalFormat("#.#");
				
				/* 갱신(충성도 없데이트) */
				sportsList.get(i).setGameNumber(sportsList.get(i).getGameNumber()+1);
				member_gameNumber = sportsList.get(i).getGameNumber();
				team_gameNumber = teamRecords.teamList.get(teamID_index).getGameNumber();
				loyality = Double.parseDouble(doubleFormat.format( (double) member_gameNumber / team_gameNumber));
				
	            double d1 = (double)member_gameNumber / team_gameNumber;
	            loyality = Double.parseDouble(doubleFormat.format(d1));
	            
				sportsList.get(i).setTeamLoyalty(loyality);
				
//				print_index(i);
				break;
			}
		}
		
		/*
		try {
			SportsDao dao = new SportsDao();
			dao.update(sportsList);
		} catch(Exception e) {
			System.out.println("SportsRecordsCreator : [ dao error ]");
			e.printStackTrace();
		}
		*/
		
	}
	
	public String sportsID(int num){
		return "Sports_PID" + formatNumber4(num);
	}
	
	public String formatNumber4(int i){
		return String.format("%04d", i);
	}

	public void print_index(int i){
		System.out.println("===Sports Records===");
			System.out.print(sportsList.get(i).getSportsID() + " ");
			System.out.print(sportsList.get(i).getTeamID() + " ");
			System.out.print(sportsList.get(i).getTeamLoyalty() + " ");
			System.out.println();
	}
	
	public void print(){
		System.out.println("===Sports Records===");
		System.out.println(sportsList.size());
		for(int i=0; i<sportsList.size(); i++){
			System.out.print(sportsList.get(i).getSportsID() + " ");
			System.out.print(sportsList.get(i).getTeamID() + " ");
			System.out.print(sportsList.get(i).getTeamLoyalty() + " ");
			System.out.println();
		}
	}
	
	public void insert() {
		try {
			SportsDao dao = new SportsDao();
			dao.update(sportsList);
		} catch(Exception e) {
			System.out.println("SportsRecordsCreator : [ dao error ]");
			e.printStackTrace();
		}
	}
}
