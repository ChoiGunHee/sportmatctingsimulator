package com.dku.mobilelab.sportmatcing.recordcreator;

import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.database.GradeDao;
import com.dku.mobilelab.sportmatcing.vo.Grade;

public class GradeRecordsCreator {
	
	ArrayList<Grade> gradeList;
	Grade grade1, grade2;
	
	public GradeRecordsCreator(){
		
		//클래스 Grade 배열
		gradeList = new ArrayList<Grade>();
		
		grade1 = new Grade("manager", "관리자");
		grade2 = new Grade("regular", "정회원");
	
		gradeList.add(grade1);
		gradeList.add(grade2);
		
		try {
			GradeDao dao = new GradeDao();
			dao.insert(gradeList);
		} catch(Exception e) {
			System.out.println("GradeRecordsCreator : [ dao error ]");
			e.printStackTrace();
		}
//		print();
	}
	
	public void print(){
		/* 출력  */
		System.out.println("-------------------Grade 데이터---------------------");
		System.out.println(gradeList.size());
		System.out.println(grade1.getGradeID() + " " + grade1.getGradeName());
		System.out.println(grade2.getGradeID() + " " + grade2.getGradeName());
		
	}
	
	public void insert() {
		
	}
}
