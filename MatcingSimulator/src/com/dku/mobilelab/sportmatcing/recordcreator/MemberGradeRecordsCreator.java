package com.dku.mobilelab.sportmatcing.recordcreator;

import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.database.MemberGradeDao;
import com.dku.mobilelab.sportmatcing.vo.MemberGrade;

public class MemberGradeRecordsCreator {
	
	ArrayList<MemberGrade> memberGradeList;

	public MemberGradeRecordsCreator(){
		
		//클래스 MemberGrade 배열
		memberGradeList = new ArrayList<MemberGrade>();
		
		TeamRecordsCreator teamRecords = null;
		int teamNumber = teamRecords.teamList.size();
		int k=0;
		
		for(int i=0; i<teamNumber; i++){
			String TeamID = teamRecords.teamList.get(i).getTeamID();
			int memberNumber = teamRecords.teamList.get(i).getMemberNumber();
			int memberID;
			
			for(memberID=k+1; memberID < k+1+memberNumber; memberID++){
				MemberGrade memberGrade;
				
				if(memberID==k+1)
					memberGrade = new MemberGrade(memberID, TeamID, "manager");
				else
					memberGrade = new MemberGrade(memberID, TeamID, "regular");
					
				memberGradeList.add(memberGrade);
			}
			k = memberID-1; 
		}

		/*
		try {
			MemberGradeDao dao = new MemberGradeDao();
			dao.insert(memberGradeList);
		} catch(Exception e) {
			System.out.println("MemberGradeRecordsCreator : [ dao error ]");
			e.printStackTrace();
		}
	*/
//		print();
	}
	
	public void print(){
		/* 출력  */
		System.out.println("-------------------MemberGrade 데이터---------------------");
		System.out.println(memberGradeList.size());
		for(int i=0; i<memberGradeList.size()/4; i++){
			System.out.print(memberGradeList.get(i).getMemberID() + " ");
			System.out.print(memberGradeList.get(i).getTeamID() + " ");
			System.out.println(memberGradeList.get(i).getGradeID());
		}
	}
}
