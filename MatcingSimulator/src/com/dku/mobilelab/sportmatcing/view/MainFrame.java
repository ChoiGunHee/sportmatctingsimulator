package com.dku.mobilelab.sportmatcing.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import com.dku.mobilelab.sportmatcing.controller.DataController;

public class MainFrame extends JFrame 
	implements ActionListener {
	
	private Button memberListBtn;
	private Button teamListBtn;
	private Button matchListBtn;
	private Button teamWeightBtn;
	private Button RecommendBtn;
	private TextArea resultArea;
	private DataController controller;
	private TextField teamIDField;


	public MainFrame(DataController controller) {
		super("SportsMatcing Recommed Project");
		this.controller = controller;
		this.setSize(850,800);
		
		//top
		Panel topPanel = new Panel(new GridLayout(2, 1));
		
		Panel top_MenuPanel  = new Panel(new GridLayout(1, 4));
		memberListBtn = new Button("Member");
		teamListBtn = new Button("Team");
		matchListBtn = new Button("Match");
		teamWeightBtn = new Button("TeamWeight");
		memberListBtn.addActionListener(this);
		teamListBtn.addActionListener(this);
		matchListBtn.addActionListener(this);
		teamWeightBtn.addActionListener(this);
		top_MenuPanel.add(memberListBtn);
		top_MenuPanel.add(teamListBtn);
		top_MenuPanel.add(matchListBtn);
		top_MenuPanel.add(teamWeightBtn);
		
		Panel top_SearchPanel = new Panel(new GridLayout(1, 2));
		RecommendBtn = new Button("Recommend Simulator Start");
		RecommendBtn.addActionListener(this);
		teamIDField = new TextField();
		top_SearchPanel.add(RecommendBtn);
		top_SearchPanel.add(teamIDField);
		
		topPanel.add(top_MenuPanel);
		topPanel.add(top_SearchPanel);
		
		//center
		Panel centerPanel = new Panel(new GridLayout(1,1));
		resultArea = new TextArea("시뮬레이터 시작");
		resultArea.setEditable(false);
		centerPanel.add(resultArea);
		
		this.add(topPanel,BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		
		this.setVisible(true);
	}
	
	public void printData(String str) {
		resultArea.setText(str);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Button btn = (Button) e.getSource();
		String result = null;
		
		switch (btn.getLabel().toString()) {
		case "Member":
			result = controller.getMemberData();
			break;
			
		case "Team":
			result = controller.getTeamData();
			break;
			
		case "Match":
			result = controller.getMetchData();
			break;
			
		case "TeamWeight":
			result = controller.getPreferceData();
			break;
			
		case "Recommend Simulator Start":
			result = controller.recommendTeam(teamIDField.getText().toString());
			
			break;
			
		default:
			break;
		}
		
		if(result == null) result = "데이터가 존재하지 않습니다.";
		
		printData(result);
	}
}
