package com.dku.mobilelab.sportmatcing.recommender;

import java.util.ArrayList;
import java.util.Collections;

import com.dku.mobilelab.sportmatcing.controller.DataController;
import com.dku.mobilelab.sportmatcing.database.PreferenceDao;
import com.dku.mobilelab.sportmatcing.database.TeamDao;
import com.dku.mobilelab.sportmatcing.vo.Preference;
import com.dku.mobilelab.sportmatcing.vo.RecommendTeam;
import com.dku.mobilelab.sportmatcing.vo.Team;

public class SportMatcingRecommender {

	private DataController controller;
	
	
	private Preference preference;

	private double fw1;
	private double fw2;
	private double fw3;
	private double alpha;
	
	private String location = "����";
	
	private ArrayList<RecommendTeam> matchingRecommendTeamList = new ArrayList<RecommendTeam>();
	
	public SportMatcingRecommender(DataController controller) {
		this.controller = controller;
		
	}

	public String cal_Similarity(String teamID, int fwNumber) {
		
		String result = "";
		
		cal_preferenceWeight(teamID);
		
		switch (fwNumber) {
			case 1:
				fw2 = 0;
				fw3 = 0;
				break;
			case 2:
				fw1 = 0;
				fw3 = 0;
				break;
			case 3:
				fw1 = 0;
				fw2 = 0;
				break;
		}
		
		TeamDao teamDao = new TeamDao();
		
		try {
			Team currentTeam = teamDao.selectTeam_ID(teamID);
			ArrayList<Team> teamList = teamDao.selectTeamListWith_Array();
			
			for (Team team : teamList) {
				if(team.getTeamID().equals(currentTeam.getTeamID())) continue;
				
				double similarity = 0;
				
				//��ġ
				int currentLocation;
				int compareLocation;
				
				if(currentTeam.getMainStadium().equals(location)) currentLocation = 0;
				else currentLocation = 1;
				
				if(team.getMainStadium().equals(location)) compareLocation = 0;
				else compareLocation = 1;
				
				double location_Similarity = fw1*( Math.pow((currentLocation-compareLocation), 2));
				
				//������ �·�
				double currentWinRate = currentTeam.getWinNumber()/currentTeam.getGameNumber();
				double compareWinRate = team.getWinNumber()/team.getGameNumber();
				
				double winningRate_Similarity = fw2*( Math.pow(currentTeam.getGameNumber() - team.getGameNumber(), 2)
														+ Math.pow(currentWinRate - compareWinRate, 2));
				
				//��ü
				double Physical_Similarity = fw3*( Math.pow(currentTeam.getAvgHeight() - team.getAvgHeight(), 2) 
														+ Math.pow(currentTeam.getAvgHeight_SD() - team.getAvgHeight_SD(), 2)
														+ Math.pow(currentTeam.getAvgWeight() - team.getAvgWeight(), 2)
														+ Math.pow(currentTeam.getAvgweight_SD() - team.getWomanRate_SD(), 2));
				
				similarity = location_Similarity + Physical_Similarity + winningRate_Similarity;
				similarity = 1/(1 + Math.sqrt(similarity));
				
				matchingRecommendTeamList.add(new RecommendTeam(team, similarity));
			}
			
			Collections.sort(matchingRecommendTeamList);
			
			result += currentTeam.toString() + "\n";
			for(int i=0; i<5; i++) {
				result += matchingRecommendTeamList.get(i).toString() + "\n";
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return result;
	}

	public String cal_preferenceWeight(String teamID) {
		PreferenceDao preferenceDao = new PreferenceDao();
		String result = "";
		try {
			preference = preferenceDao.selectPreference_ID(teamID);
			
			double totalFw = preference.getFw1() + preference.getFw2() + preference.getFw3();
			fw1 = preference.getFw1()/totalFw;
			fw2 = preference.getFw2()/totalFw;
			fw3 = preference.getFw3()/totalFw;
			alpha = totalFw/preference.getMc();
			
			result += "fw1 : " + fw1 + "\n";
			result += "fw2 : " + fw2 + "\n";
			result += "fw3 : " + fw3 + "\n";
			result += "alpha : " + alpha + "\n";
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	public String cal_Similarity_Location(String teamID) {
		cal_preferenceWeight(teamID);
		matchingRecommendTeamList.clear();
		return cal_Similarity(teamID, 1);
	}

	public String cal_Similarity_WinningRate(String teamID) {
		cal_preferenceWeight(teamID);
		matchingRecommendTeamList.clear();
		return cal_Similarity(teamID, 2);
	}

	public String cal_Similarity_Physical(String teamID) {
		cal_preferenceWeight(teamID);
		matchingRecommendTeamList.clear();
		return cal_Similarity(teamID, 3);
	}

	public String recommende(String teamID) {
		String result = "";
		
		cal_preferenceWeight(teamID);
		matchingRecommendTeamList.clear();
		cal_Similarity(teamID, 0);
		for (RecommendTeam recommendTeam : matchingRecommendTeamList) {
			recommendTeam.setSimilarity( alpha * recommendTeam.getSimilarity() + (1-alpha) * recommendTeam.getTeam().getRating());
		}
		
		Collections.sort(matchingRecommendTeamList);
		for(int i=0; i<5; i++) {
			result += matchingRecommendTeamList.get(i).toString() + "\n";
		}
		
		return result;
	}
	
	
}
