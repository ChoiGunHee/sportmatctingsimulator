package com.dku.mobilelab.sportmatcing.vo;

public class Preference {
	private String teamID;
	private int fw1;
	private int fw2;
	private int fw3;
	private int mc;
	private double averageRating;
	
	public Preference() {
		
	}
	
	public Preference(String teamID, int fw1, int fw2, int fw3, int mc, double averageRating) {
		super();
		this.teamID = teamID;
		this.fw1 = fw1;
		this.fw2 = fw2;
		this.fw3 = fw3;
		this.mc = mc;
		this.averageRating = averageRating;
	}

	public String getTeamID() {
		return teamID;
	}
	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}
	public int getFw1() {
		return fw1;
	}
	public void setFw1(int fw1) {
		this.fw1 = fw1;
	}
	public int getFw2() {
		return fw2;
	}
	public void setFw2(int fw2) {
		this.fw2 = fw2;
	}
	public int getFw3() {
		return fw3;
	}
	public void setFw3(int fw3) {
		this.fw3 = fw3;
	}
	public int getMc() {
		return mc;
	}
	public void setMc(int mc) {
		this.mc = mc;
	}
	public double getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}
	
	
}
