package com.dku.mobilelab.sportmatcing.vo;

import java.util.Comparator;

public class RecommendTeam 
	implements Comparable<RecommendTeam> {
	private Team team;
	private double similarity;
	
	public RecommendTeam() {
		
	}
	
	@Override
	public String toString() {
		return "RecommendTeam [team=" + team + ", similarity=" + similarity + "]";
	}

	public RecommendTeam(Team team, double similarity) {
		super();
		this.team = team;
		this.similarity = similarity;
	}

	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	@Override
	public int compareTo(RecommendTeam o) {
		if(this.similarity < o.similarity) return 1;
		else if(this.similarity < o.similarity) return -1;
		else return 0;
	}

	
}
