package com.dku.mobilelab.sportmatcing.vo;
public class Stadium {
	
	private String stadiumID;
	private double coordinateX;
	private double coordinateY;
	
	public Stadium(String stadiumID, double coordinateX, double coordinateY) {
		this.stadiumID = stadiumID;
		this.coordinateX = coordinateX;
		this.coordinateY = coordinateY;
	}
	
	public String getStadiumID() {
		return stadiumID;
	}
	public void setStadiumID(String stadiumID) {
		this.stadiumID = stadiumID;
	}
	public double getCoordinateX() {
		return coordinateX;
	}
	public void setCoordinateX(double coordinateX) {
		this.coordinateX = coordinateX;
	}
	public double getCoordinateY() {
		return coordinateY;
	}
	public void setCoordinateY(double coordinateY) {
		this.coordinateY = coordinateY;
	}
}
