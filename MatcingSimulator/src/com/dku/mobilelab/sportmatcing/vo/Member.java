package com.dku.mobilelab.sportmatcing.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.Random;

import com.dku.mobilelab.sportmatcing.excel.NameExcelFileReader;

/* 
 * (삭제)
 * String teamID  
 * (변경)
 * String id -> String memberID
 * String pw -> String memberPW
 * int height -> double height
 * int weight -> double weight
 * */

public class Member {
	private String memberID;
	private String memberPW;
	private String sportsID; //축구 정보 ID(일단 제외)
	private String name;
	private String gender;
	private Date birthDate; //생년월일
	private String phone;
	private String email;
	private String location;
	private double height; //신장
	private double weight; //체중
	private String disability; //장애 여부

	//생성자
	public Member(int j, int memberID, String sportsID, String gender, 
			double avgAge, String location, String disability) throws ParseException {
		Random random = new Random();
		
		this.memberID = "PID" + formatNumber4(memberID);
		this.memberPW = "1234";
		this.sportsID = sportsID;
		name = randomName(random.nextInt(329)); //랜덤으로 이름을 지정하는 함수
		this.gender = gender;
		birthDate = randomAge((int)avgAge); //랜덤으로 나이를 지정하는 함수
		phone = "010-" + formatNumber4(memberID) + "-" + formatNumber4(memberID);
		email = formatNumber4(memberID)+"@test.com";
		this.location = location;
		
		// 장애 여부
		// (disability="Y"이면, disability="Y")
		// (disability="N"이면, 랜덤에 따라 "Y"로 지정(단, "N" 비율이 더 커야한다.)
		if(disability == "Y") 
			this.disability = "Y";
		else { //disability = "N"인 경우
			if(j>=10) //최대 18명의 멤버로 구성될 수 있는데, 반 이상 "N"이어야하므로 간단히 10부터 랜덤으로 "Y"를 지정  
				this.disability = (random.nextInt(2) == 1) ? "Y" : "N"; //random값이 1이면 "Y"로 지정.
			else
				this.disability = "N";
		}
		
		//키, 몸무게
		height = (gender == "M") ? random.nextInt(41)+150 : random.nextInt(41)+140;
		weight = random.nextInt(20) + (int) ((height-100)*0.9 - 10); //표준체중 +- 10을 범위로 함.
		
		}
	
	public Member() {
		// TODO Auto-generated constructor stub
	}

	public String formatNumber4(int i){
		return String.format("%04d", i);
	}
	
	public String randomName(int i){
		
		NameExcelFileReader reader = new NameExcelFileReader();
		ArrayList<String> teamList = reader.readFile("namelist.xls");
		
		return teamList.get(i);
	}
	
	public Date randomAge(int age) throws ParseException{
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Random random = new Random();
		int curYear = Calendar.getInstance().get(Calendar.getInstance().YEAR); //현재 년도
		int year, month, day;
		
		//팀의 평균나이에 따라 연도를 랜덤으로 지정
		// ex)팀의 평균나이가 -> 20(20대), year는 1997(20살)~1988(29살)
		year =(curYear - age + 1) - random.nextInt(10) ;
		month = random.nextInt(12) + 1;
		day = randomDay(month);
		
		cal.set(year, month, day);
		
		String temp = sdf.format(cal.getTime());
		
		return new Date(sdf.parse(temp).getTime());		
	}
	
	public int randomDay(int month){
		int day; 
		Random random = new Random();
		
		switch(month){
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = random.nextInt(31) + 1;
			break;
		case 2:
			day = random.nextInt(28) + 1;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = random.nextInt(30) + 1;
			break;
			default:
				day = 0;
				break;
		}
		return day;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getMemberPW() {
		return memberPW;
	}

	public void setMemberPW(String memberPW) {
		this.memberPW = memberPW;
	}

	public String getSportsID() {
		return sportsID;
	}

	public void setSportsID(String sportsID) {
		this.sportsID = sportsID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDisability() {
		return disability;
	}

	public void setDisability(String disability) {
		this.disability = disability;
	}

	
}
