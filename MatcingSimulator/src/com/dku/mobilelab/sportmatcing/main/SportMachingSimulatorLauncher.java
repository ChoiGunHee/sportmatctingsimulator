package com.dku.mobilelab.sportmatcing.main;

import java.util.ArrayList;
import java.util.Random;

import com.dku.mobilelab.sportmatcing.controller.DataController;
import com.dku.mobilelab.sportmatcing.database.MatchDao;
import com.dku.mobilelab.sportmatcing.database.MemberDao;
import com.dku.mobilelab.sportmatcing.database.PreferenceDao;
import com.dku.mobilelab.sportmatcing.database.SportsDao;
import com.dku.mobilelab.sportmatcing.database.TeamDao;
import com.dku.mobilelab.sportmatcing.recordcreator.GradeRecordsCreator;
import com.dku.mobilelab.sportmatcing.recordcreator.MatchRecordsCreator;
import com.dku.mobilelab.sportmatcing.recordcreator.MemberGradeRecordsCreator;
import com.dku.mobilelab.sportmatcing.recordcreator.TeamRecordsCreator;
import com.dku.mobilelab.sportmatcing.vo.Match;
import com.dku.mobilelab.sportmatcing.vo.Preference;
import com.dku.mobilelab.sportmatcing.vo.Sports;
import com.dku.mobilelab.sportmatcing.vo.Team;

public class SportMachingSimulatorLauncher {
	
	public static void main(String [] args) {
		//SportMachingSimulatorLauncher.dataCreate();
		
		DataController dataController = new DataController();
	}
	
	private static void dataCreate() {
		//기본 데이터 생성
		GradeRecordsCreator gradeRecords = new GradeRecordsCreator();
		System.out.println("GradeRecords Complete");
		
		TeamRecordsCreator teamRecords = new TeamRecordsCreator();
		System.out.println("teamRecords Complete");
		
		MemberGradeRecordsCreator memberGradeRecords = new MemberGradeRecordsCreator();
		System.out.println("memberGradeRecords Complete");
		
		MatchRecordsCreator matchRecords = new MatchRecordsCreator();
		System.out.println("matchRecords Complete");
		
		//스포츠List 생성
		ArrayList<Sports> list = teamRecords.sportsList;
		try {
			SportsDao dao = new SportsDao();
			dao.update(list);
		} catch(Exception e) {
			System.out.println("SportsRecordsCreator : [ dao error ]");
			e.printStackTrace();
		}
		System.out.println("SportsRecords Complete");
		
		//경기수, 이긴수 생성
		MatchDao matchDao = new MatchDao();
		TeamDao teamDao = new TeamDao();
		try {
			ArrayList<Team> teamList = teamDao.selectTeamListWith_Array();
			
			int winningGameCount = 0;
			for (Team team : teamList) {
				ArrayList<Match> teamMatchList = matchDao.selectMatchList_Team(team.getTeamID().toString());
				for (Match match : teamMatchList) {
					if(match.getWinningTeam() == null) continue;
					else if(match.getWinningTeam().equals(team.getTeamID()))
						winningGameCount++;
				}
				team.setGameNumber(teamMatchList.size()/11);
				team.setWinNumber(winningGameCount/11);
				teamDao.update_gameNumber(team);
				winningGameCount = 0;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("GameNumber Complete");
		
		//팀 평점 생성
		MemberDao memberDao = new MemberDao();
		SportsDao sportsDao = new SportsDao();
		try {
			ArrayList<Team> teamList = teamDao.selectTeamListWith_Array();
			
			double totalRating = 0;
			for (Team team : teamList) {
				ArrayList<Match> teamMatchList = matchDao.selectMatchList_TeamB(team.getTeamID().toString());
				for (Match match : teamMatchList) {
					String memberSportID = memberDao.selectMembe_ID(match.getMemberID()).getSportsID();
					double memberteamLoyalty = sportsDao.selectSport_ID(memberSportID).getTeamLoyalty();
					totalRating += (match.getRating()*memberteamLoyalty);
				}
				team.setRating(totalRating/teamMatchList.size());
				teamDao.update_rating(team);
				totalRating = 0;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Rating Complete");
		
		//가중치 초기 생성
		PreferenceDao preferenceDao = new PreferenceDao();
		
		try {
			Random random = new Random();
			ArrayList<Team> teamList = teamDao.selectTeamListWith_Array();
			ArrayList<Preference> preferenceList = new ArrayList<Preference>();
			
			for (Team team : teamList) {
				Preference preference = new Preference();
				preference.setTeamID(team.getTeamID());
				preference.setMc(team.getGameNumber() + 3);
				preference.setAverageRating(team.getRating());
				
				int fw1=1, fw2=1, fw3=1;
				for(int i=0; i<team.getGameNumber(); i++) {
					switch (random.nextInt(4)) {
					case 1:
						fw1++;
						break;
						
					case 2:
						fw2++;
						break;
						
					case 3:
						fw3++;
						break;

					}
				}
				preference.setFw1(fw1);
				preference.setFw2(fw2);
				preference.setFw3(fw3);
				preferenceList.add(preference);
			}
			
			preferenceDao.insert_init(preferenceList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Preference Complete");
		
		System.out.println("Complete!!");
	}
}