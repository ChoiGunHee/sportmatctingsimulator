package com.dku.mobilelab.sportmatcing.controller;

import com.dku.mobilelab.sportmatcing.database.MatchDao;
import com.dku.mobilelab.sportmatcing.database.MemberDao;
import com.dku.mobilelab.sportmatcing.database.PreferenceDao;
import com.dku.mobilelab.sportmatcing.database.TeamDao;
import com.dku.mobilelab.sportmatcing.recommender.SportMatcingRecommender;
import com.dku.mobilelab.sportmatcing.view.MainFrame;
import com.dku.mobilelab.sportmatcing.vo.Preference;

public class DataController {

	private MainFrame mainFrame;
	private SportMatcingRecommender recommender;

	public DataController() {
		mainFrame = new MainFrame(this);
		recommender = new SportMatcingRecommender(this);
	}

	public String getMemberData() {
		MemberDao memberDao = new MemberDao();
		try {
			return memberDao.selectMemberList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public String getTeamData() {
		TeamDao teamDao = new TeamDao();
		try {
			return teamDao.selectTeamList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getMetchData() {
		MatchDao matchDao = new MatchDao();
		try {
			return matchDao.selectMatchList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getPreferceData() {
		PreferenceDao preferenceDao = new PreferenceDao();
		try {
			return preferenceDao.selectPreferenceList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String recommendTeam(String teamID) {
		String result = "";
		
		result += "�� ��ȣ�� ����ġ ���...\n";
		result += recommender.cal_preferenceWeight(teamID);
		result += "\n\n";
		
		result += "���� ���絵 ���� 5����...\n";
		result += recommender.cal_Similarity(teamID,0);
		result += "\n";
		
		result += "���� ��Ī ���� 5����...\n";
		result += recommender.recommende(teamID);
		result += "\n";
		
		result += "��ġ ���絵 ���� 5����...\n";
		result += recommender.cal_Similarity_Location(teamID);
		result += "\n";
		
		result += "�·��� ���� ���絵 ���� 5����...\n";
		result += recommender.cal_Similarity_WinningRate(teamID);
		result += "\n";
		
		result += "��ü ���絵 ���� 5����...\n";
		result += recommender.cal_Similarity_Physical(teamID);
		result += "\n";
		return result;
	}
	
	
}
