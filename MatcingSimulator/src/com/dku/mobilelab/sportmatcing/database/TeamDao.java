package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Sports;
import com.dku.mobilelab.sportmatcing.vo.Team;

public class TeamDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<Team> teamList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into TeamTable(teamID, teamName, openingDate, mainStadium, genderRatio, "
				   + "genderStandardDeviation, disabilityRatio, disabilityStandardDeviation, numberOfMatch, "
				   + "numberOfWinning, ageAverage, ageStandardDeviation, rating, heightAverage, heightStandardDeviation, "
				   + "weightAverage, weightStandardDeviation, numberOfMember) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			for(Team vo : teamList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getTeamID());
				pstmt.setString(2, vo.getTeamName());
				pstmt.setDate(3, vo.getOpeningDate());
				pstmt.setString(4, vo.getMainStadium());
				pstmt.setDouble(5, vo.getManRate());
				pstmt.setDouble(6, vo.getManRate_SD());
				pstmt.setDouble(7, vo.getDisabilityRate());
				pstmt.setDouble(8, vo.getDisability_SD());
				pstmt.setInt(9, vo.getGameNumber());
				pstmt.setInt(10, vo.getWinNumber());
				pstmt.setDouble(11, vo.getAvgAge());
				pstmt.setDouble(12, vo.getAvgAge_SD());
				pstmt.setDouble(13, vo.getRating());
				pstmt.setDouble(14, vo.getAvgHeight());
				pstmt.setDouble(15, vo.getAvgHeight_SD());
				pstmt.setDouble(16, vo.getAvgWeight());
				pstmt.setDouble(17, vo.getAvgweight_SD());
				pstmt.setInt(18, vo.getMemberNumber());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("TeamDao : [ insert error ]");
			e.printStackTrace();
			conn.close();
		}
	}

	public String selectTeamList() throws Exception {
		conn = dbc.getDBConnection();
		String result = "";
		
		String sql = "select * from TeamTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				result += rs.getString("teamID") + "\t";
				result += rs.getString("teamName") + "\t";
				result += rs.getString("mainStadium") + "\t";
				result += rs.getDate("openingDate").toString() + "\t";
				result += rs.getDouble("genderRatio") + "\t";
				result += rs.getDouble("disabilityRatio") + "\t";
				result += rs.getInt("numberOfMatch") + "\t";
				result += rs.getInt("numberOfWinning") + "\t";
				result += rs.getDouble("rating") + "\t";
				result += rs.getInt("numberOfMember") + "\t";
				result += rs.getDouble("heightAverage") + "\t";
				result += rs.getDouble("weightAverage") + "\t";
				result += "\n";
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	
	public ArrayList<Team> selectTeamListWith_Array() throws Exception {
		conn = dbc.getDBConnection();
		ArrayList<Team> result = new ArrayList<Team>();
		
		String sql = "select * from TeamTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				Team team = new Team();
				team.setTeamID(rs.getString("teamID"));
				team.setTeamName(rs.getString("teamName"));
				team.setOpeningDate(rs.getDate("openingDate"));
				team.setMainStadium(rs.getString("mainStadium"));
				team.setManRate(rs.getDouble("genderRatio"));
				team.setManRate_SD(rs.getDouble("genderStandardDeviation"));
				team.setDisabilityRate(rs.getDouble("disabilityRatio"));
				team.setDisability_SD(rs.getDouble("disabilityStandardDeviation"));
				team.setGameNumber(rs.getInt("numberOfMatch"));
				team.setWinNumber(rs.getInt("numberOfWinning"));
				team.setAvgAge(rs.getDouble("ageAverage"));
				team.setAvgAge_SD(rs.getDouble("ageStandardDeviation"));
				team.setRating(rs.getDouble("rating"));
				team.setAvgHeight(rs.getDouble("heightAverage"));
				team.setAvgHeight_SD(rs.getDouble("heightStandardDeviation"));
				team.setAvgWeight(rs.getDouble("weightAverage"));
				team.setAvgAge_SD(rs.getDouble("weightStandardDeviation"));
				team.setMemberNumber(rs.getInt("numberOfMember"));
				result.add(team);
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}

	public void update_gameNumber(Team vo) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "update TeamTable set numberOfMatch = ?, numberOfWinning = ? where teamID = ?";
		
		try {
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, vo.getGameNumber());
				pstmt.setInt(2, vo.getWinNumber());
				pstmt.setString(3, vo.getTeamID());
				pstmt.executeUpdate();
				
		} catch(SQLException e) {
			System.out.println("TeamDao : [ update error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}

	public void update_rating(Team vo) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "update TeamTable set rating = ? where teamID = ?";
		
		try {
				pstmt = conn.prepareStatement(sql);
				pstmt.setDouble(1, vo.getRating());
				pstmt.setString(2, vo.getTeamID());
				pstmt.executeUpdate();
				
		} catch(SQLException e) {
			System.out.println("TeamDao : [ update error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
	}

	public Team selectTeam_ID(String teamID) throws Exception {
		conn = dbc.getDBConnection();
		Team result = new Team();
		
		String sql = "select * from TeamTable where teamID = ?;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, teamID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				result.setTeamID(rs.getString("teamID"));
				result.setTeamName(rs.getString("teamName"));
				result.setOpeningDate(rs.getDate("openingDate"));
				result.setMainStadium(rs.getString("mainStadium"));
				result.setManRate(rs.getDouble("genderRatio"));
				result.setManRate_SD(rs.getDouble("genderStandardDeviation"));
				result.setDisabilityRate(rs.getDouble("disabilityRatio"));
				result.setDisability_SD(rs.getDouble("disabilityStandardDeviation"));
				result.setGameNumber(rs.getInt("numberOfMatch"));
				result.setWinNumber(rs.getInt("numberOfWinning"));
				result.setAvgAge(rs.getDouble("ageAverage"));
				result.setAvgAge_SD(rs.getDouble("ageStandardDeviation"));
				result.setRating(rs.getDouble("rating"));
				result.setAvgHeight(rs.getDouble("heightAverage"));
				result.setAvgHeight_SD(rs.getDouble("heightStandardDeviation"));
				result.setAvgWeight(rs.getDouble("weightAverage"));
				result.setAvgAge_SD(rs.getDouble("weightStandardDeviation"));
				result.setMemberNumber(rs.getInt("numberOfMember"));
			}
			
		
		} catch(SQLException e) {
			System.out.println("TeamDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
}
