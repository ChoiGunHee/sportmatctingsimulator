package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Match;

public class MatchDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<Match> matchList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into matchtable(memberID, teamID, " 
				 	 + "matchDate, teamA, teamB, stadiumID, teamAScore, teamBScore, "
				 	 + "winningTeam, ratingTeam) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			for(Match vo : matchList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getMemberID());
				pstmt.setString(2, vo.getTeamID());
				pstmt.setDate(3, vo.getMatchDate());
				pstmt.setString(4, vo.getTeamA());
				pstmt.setString(5, vo.getTeamB());
				pstmt.setString(6, vo.getStadiumID());
				pstmt.setInt(7, vo.getTeamAScore());
				pstmt.setInt(8, vo.getTeamBScore());
				pstmt.setString(9, vo.getWinningTeam());
				pstmt.setDouble(10, vo.getRating());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("MatchDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	
	public String selectMatchList() throws Exception {
		conn = dbc.getDBConnection();
		String result = "";
		
		String sql = "select * from MatchTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				result += rs.getInt("matchID") + "\t";
				result += rs.getString("memberID") + "\t";
				result += rs.getString("teamID") + "\t";
				result += rs.getDate("matchDate").toString() + "\t";
				result += rs.getString("teamA") + "\t";
				result += rs.getString("teamB") + "\t";
				result += rs.getString("stadiumID") + "\t";
				result += rs.getInt("teamAScore") + "\t";
				result += rs.getInt("teamBScore") + "\t";
				result += rs.getString("winningTeam") + "\t";
				result += rs.getDouble("ratingTeam") + "\t";
				result += "\n";
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	public ArrayList<Match> selectMatchList_Array() throws Exception {
		conn = dbc.getDBConnection();
		 ArrayList<Match> result = new ArrayList<Match>();
		
		String sql = "select * from MatchTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				Match match = new Match();
				match.setMatchId(rs.getInt("matchID"));
				match.setMemberID(rs.getString("memberID"));
				match.setTeamID(rs.getString("teamID"));
				match.setMatchDate(rs.getDate("matchDate"));
				match.setTeamA(rs.getString("teamA"));
				match.setTeamB(rs.getString("teamB"));
				match.setStadiumID(rs.getString("stadiumID"));
				match.setTeamAScore(rs.getInt("teamAScore"));
				match.setTeamBScore(rs.getInt("teamBScore"));
				match.setWinningTeam(rs.getString("winningTeam"));
				match.setRating(rs.getDouble("ratingTeam"));
				result.add(match);
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	public ArrayList<Match> selectMatchList_Team(String teamId) throws Exception {
		conn = dbc.getDBConnection();
		 ArrayList<Match> result = new ArrayList<Match>();
		
		String sql = "select * from MatchTable where teamID = ?";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, teamId);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				Match match = new Match();
				match.setMatchId(rs.getInt("matchID"));
				match.setMemberID(rs.getString("memberID"));
				match.setTeamID(rs.getString("teamID"));
				match.setMatchDate(rs.getDate("matchDate"));
				match.setTeamA(rs.getString("teamA"));
				match.setTeamB(rs.getString("teamB"));
				match.setStadiumID(rs.getString("stadiumID"));
				match.setTeamAScore(rs.getInt("teamAScore"));
				match.setTeamBScore(rs.getInt("teamBScore"));
				match.setWinningTeam(rs.getString("winningTeam"));
				match.setRating(rs.getDouble("ratingTeam"));
				result.add(match);
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	public ArrayList<Match> selectMatchList_TeamB(String teamBId) throws Exception {
		conn = dbc.getDBConnection();
		 ArrayList<Match> result = new ArrayList<Match>();
		
		String sql = "select * from MatchTable where teamB = ?";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, teamBId);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				Match match = new Match();
				match.setMatchId(rs.getInt("matchID"));
				match.setMemberID(rs.getString("memberID"));
				match.setTeamID(rs.getString("teamID"));
				match.setMatchDate(rs.getDate("matchDate"));
				match.setTeamA(rs.getString("teamA"));
				match.setTeamB(rs.getString("teamB"));
				match.setStadiumID(rs.getString("stadiumID"));
				match.setTeamAScore(rs.getInt("teamAScore"));
				match.setTeamBScore(rs.getInt("teamBScore"));
				match.setWinningTeam(rs.getString("winningTeam"));
				match.setRating(rs.getDouble("ratingTeam"));
				result.add(match);
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
}
