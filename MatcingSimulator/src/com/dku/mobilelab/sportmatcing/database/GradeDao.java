package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Grade;

public class GradeDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<Grade> gradeList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert ignore into GradeTable(gradeID, " 
				 	 + "gradeName) values(?, ?)";
		
		try {
			for(Grade vo : gradeList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getGradeID());
				pstmt.setString(2, vo.getGradeName());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("GradeDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
}
