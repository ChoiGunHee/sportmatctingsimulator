package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Grade;
import com.dku.mobilelab.sportmatcing.vo.Preference;
import com.dku.mobilelab.sportmatcing.vo.Team;

public class PreferenceDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert_init(ArrayList<Preference> preferenceList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into PreferenceTable(TeamID, FW1, FW2, FW3, MC, averageRating) values(?, ?, ?, ?, ?, ?)";
		
		try {
			for(Preference vo : preferenceList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getTeamID());
				pstmt.setInt(2, vo.getFw1());
				pstmt.setInt(3, vo.getFw2());
				pstmt.setInt(4, vo.getFw3());
				pstmt.setInt(5, vo.getMc());
				pstmt.setDouble(6, vo.getAverageRating());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("PreferenceDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	public String selectPreferenceList() throws Exception {
		conn = dbc.getDBConnection();
		String result = "";
		
		String sql = "select * from PreferenceTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				result += rs.getString("TeamID") + "\t";
				result += rs.getInt("FW1") + "\t";
				result += rs.getInt("FW2") + "\t";
				result += rs.getInt("FW3") + "\t";
				result += rs.getInt("MC") + "\t";
				result += rs.getDouble("averageRating") + "\t";
				result += "\n";
			}
			
		
		} catch(SQLException e) {
			System.out.println("PreferenceDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	public Preference selectPreference_ID(String teamID) throws Exception {
		conn = dbc.getDBConnection();
		Preference result = new Preference();
		
		String sql = "select * from PreferenceTable where TeamID = ?;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, teamID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				result.setTeamID(rs.getString("TeamID"));
				result.setFw1(rs.getInt("FW1"));
				result.setFw2(rs.getInt("FW2"));
				result.setFw3(rs.getInt("FW3"));
				result.setMc(rs.getInt("MC"));
				result.setAverageRating(rs.getDouble("averageRating"));
			}
			
		
		} catch(SQLException e) {
			System.out.println("PreferenceDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
}
