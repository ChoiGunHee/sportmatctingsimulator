package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Member;

public class MemberDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<Member> memberList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into membertable(memberID, memberPassword, sportsID, " 
				 	 + "memberName, gender, birthDate, phoneNumber, emailAddress, mainStadium, "
				 	 + "height, weight, disability) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			for(Member vo : memberList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getMemberID());
				pstmt.setString(2, vo.getMemberPW());
				pstmt.setString(3, vo.getSportsID());
				pstmt.setString(4, vo.getName());
				pstmt.setString(5, vo.getGender());
				pstmt.setDate(6, vo.getBirthDate());
				pstmt.setString(7, vo.getPhone());
				pstmt.setString(8, vo.getEmail());
				pstmt.setString(9, vo.getLocation());
				pstmt.setDouble(10, vo.getHeight());
				pstmt.setDouble(11, vo.getWeight());
				pstmt.setString(12, vo.getDisability());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("MemberDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	public String selectMemberList() throws Exception {
		conn = dbc.getDBConnection();
		String result = "";
		
		String sql = "select * from MemberTable;";
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery(sql.toString());
			
			while (rs.next()) {
				result += rs.getString("memberID") + "\t";
				//result += rs.getString("memberPassword") + "\t\t";
				result += rs.getString("sportsID") + "\t";
				result += rs.getString("memberName") + "\t";
				result += rs.getString("gender") + "\t";
				result += rs.getDate("birthDate").toString() + "\t";
				//result += rs.getString("phoneNumber") + "\t";
				//result += rs.getString("emailAddress") + "\t";
				result += rs.getString("mainStadium") + "\t";
				result += rs.getDouble("height") + "\t";
				result += rs.getDouble("weight") + "\t";
				result += rs.getString("disability") + "\t";
				result += "\n";
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
	
	public Member selectMembe_ID(String memberID) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "select * from MemberTable where memberID = ?;";
		ResultSet rs = null;
		
		Member result = new Member();
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				result.setMemberID(rs.getString("memberID"));
				result.setName(rs.getString("memberName"));
				result.setSportsID(rs.getString("sportsID"));
				result.setGender(rs.getString("gender"));
				result.setBirthDate(rs.getDate("birthDate"));
				result.setHeight(rs.getDouble("height"));
				result.setWeight(rs.getDouble("weight"));
				result.setDisability(rs.getString("disability"));
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
}
