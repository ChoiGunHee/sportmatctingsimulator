package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Member;
import com.dku.mobilelab.sportmatcing.vo.MemberGrade;

public class MemberGradeDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<MemberGrade> memberGradeList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into membergradetable(memberID, teamID, " 
				 	 + "gradeID) values(?, ?, ?)";
		
		try {
			for(MemberGrade vo : memberGradeList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getMemberID());
				pstmt.setString(2, vo.getTeamID());
				pstmt.setString(3, vo.getGradeID());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("MemberGradeDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
}
