package com.dku.mobilelab.sportmatcing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dku.mobilelab.sportmatcing.vo.Member;
import com.dku.mobilelab.sportmatcing.vo.Sports;

public class SportsDao {
	private Connection conn = null;
	private SportMatcingDBConnector dbc = new SportMatcingDBConnector();
	private PreparedStatement pstmt = null;
	
	public void insert(ArrayList<Sports> sportsList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "insert into sportstable(sportsID, teamID, teamLoyalty) "
				   + "values(?, ?, ?)";
		
		try {
			for(Sports vo : sportsList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, vo.getSportsID());
				pstmt.setString(2, vo.getTeamID());
				pstmt.setDouble(3, vo.getTeamLoyalty());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("SportsDao : [ insert error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	public void update(ArrayList<Sports> sportsList) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "update sportstable set teamLoyalty = ?  where sportsID = ? and teamID = ?";
		
		try {
			for(Sports vo : sportsList) {
				pstmt = conn.prepareStatement(sql);
				pstmt.setDouble(1, vo.getTeamLoyalty());
				pstmt.setString(2, vo.getSportsID());
				pstmt.setString(3, vo.getTeamID());
				pstmt.executeUpdate();
			}
		} catch(SQLException e) {
			System.out.println("SportsDao : [ update error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
	
	public Sports selectSport_ID(String sportsId) throws Exception {
		conn = dbc.getDBConnection();
		
		String sql = "select * from SportsTable where sportsID = ?;";
		ResultSet rs = null;
		
		Sports result = new Sports();
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sportsId);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				result.setSportsID(rs.getString("sportsID"));
				result.setTeamID(rs.getString("teamID"));
				result.setTeamLoyalty(rs.getDouble("teamLoyalty"));
			}
			
		
		} catch(SQLException e) {
			System.out.println("MemberDao : [ select error ]");
			e.printStackTrace();
		} finally {
			conn.close();
		}
		
		return result;
	}
}
