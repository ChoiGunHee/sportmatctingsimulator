package com.dku.mobilelab.sportmatcing.test;

import java.util.ArrayList;

import org.junit.Test;

import com.dku.mobilelab.sportmatcing.excel.NameExcelFileReader;

public class NameExcelFileReaderTest {

	@Test
	public void readNameExcelFileTest() {
		NameExcelFileReader reader = new NameExcelFileReader();
		ArrayList<String> list = reader.readFile("namelist.xls");
		for (String string : list) {
			System.out.println(string);
		}
		
	}
	
}
