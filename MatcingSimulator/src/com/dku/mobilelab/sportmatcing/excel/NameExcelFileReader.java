package com.dku.mobilelab.sportmatcing.excel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class NameExcelFileReader {
	
	public ArrayList<String> readFile(String filename)
	{
		ArrayList<String> nameList = new ArrayList<String>();
		
		Workbook wb = null;
		String path = getClass().getResource("").getPath();
		File file = new File(path + filename);
		
		try {
			wb = Workbook.getWorkbook(file);
            Sheet s = wb.getSheet(0);
            int row = 329; //s.getRows();
            int col = 1; //s.getColumns();

            for(int i=0; i<row; i++){
                for(int j=0; j<col; j++){
                    Cell c = s.getCell(j, i);
                    nameList.add(c.getContents());
                }
            }
		} catch (BiffException | IOException e) {
			e.printStackTrace();
		}
		
		return nameList;
	}
}
