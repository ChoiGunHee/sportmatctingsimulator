create database sportsDB;

use sportsDB;

create table StadiumTable (
	stadiumID		varchar(20)	not null,
	coordinateX		double,
    coordinateY		double,
    constraint pk_stadium primary key(stadiumID)
);

create table TeamTable (
	teamID						varchar(20)	not null,
	teamName					varchar(30),
    openingDate					date,
    mainStadium					varchar(20),
    genderRatio					double,
    genderStandardDeviation		double,
    disabilityRatio				double,
    disabilityStandardDeviation	double,
    numberOfMatch				int,
    numberOfWinning				int,
    ageAverage					double,
    ageStandardDeviation		double,
    rating						double,
    heightAverage				double,
    heightStandardDeviation		double,
    weightAverage				double,
    weightStandardDeviation		double,
    numberOfMember				int,
    constraint pk_team primary key(teamID),
    foreign key(mainStadium) references StadiumTable(stadiumID)
);

create table SportsTable (
	sportsID		varchar(20)	not null,
    teamID			varchar(20)	not null,
    teamLoyalty		double,
    constraint pk_sports primary key(sportsID),
    foreign key(teamID) references TeamTable(teamID)
);

create table MemberTable (
	memberID		varchar(20)	not null,
    memberPassword	varchar(20),
    sportsID		varchar(20),
    memberName		varchar(30),
    gender			char(1),
    birthDate		date,
    phoneNumber		varchar(20),
    emailAddress	varchar(30),
    mainStadium		varchar(20),
    height			double,
    weight			double,
    disability		char(1),
    constraint pk_member primary key(memberID),
    foreign key(sportsID) references SportsTable(sportsID),
    foreign key(mainStadium) references StadiumTable(stadiumID)
);

create table GradeTable (
	gradeID 	varchar(20)	not null,
    gradeName	varchar(30),
    constraint pk_grade primary key(gradeID)
);

create table MatchTable (
	matchID			int	auto_increment	not null,
    memberID		varchar(20),
    teamID			varchar(20),
    matchDate		date,
    teamA			varchar(30),
    teamB			varchar(30),
    stadiumID		varchar(20),
    teamAScore		int,
    teamBScore		int,
    winningTeam		varchar(30),
    ratingTeam		double,
    constraint pk_match primary key(matchID),
    foreign key(memberID) references MemberTable(memberID),
    foreign key(teamID) references TeamTable(teamID),
    foreign key(stadiumID) references StadiumTable(stadiumID)
);

create table PreferenceTable (
	TeamID			varchar(20)	not null,
    FW1				double,
	FW2				double,
    FW3				double,
    averageRating	double,
    constraint pk_preferenceTable primary key(TeamID),
    foreign key(TeamID) references TeamTable(TeamID)
);

create table MemberGradeTable (
	memberGradeID	int	auto_increment	not null,
	memberID		varchar(20)	not null,
    teamID			varchar(20)	not null,
    gradeID 		varchar(20)	not null,
    constraint pk_match primary key(memberGradeID),
    foreign key(memberID) references MemberTable(memberID),
	foreign key(teamID) references TeamTable(TeamID),
    foreign key(gradeID) references GradeTable(gradeID)
);

insert into StadiumTable(stadiumID, coordinateX, coordinateY) values('성남', 1.98989, 2.8989);
insert into StadiumTable(stadiumID, coordinateX, coordinateY) values('서울', 5.45454, 34343.3);



select * from GradeTable;
select * from TeamTable;
select * from SportsTable;
select * from MemberTable;
select * from MemberGradeTable;
select * from MatchTable;

drop database sportsdb;