package jdbcConntector;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import table.Member;
import table.Team;

public class JDBCConnector {

	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://127.0.0.1:3306/testdb?useSSL=false";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "3469";

	public void insertRecordIntoTable(ArrayList<Team> teamList,
			ArrayList<Member> memberList) throws SQLException {

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		String insertTableSQL = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			dbConnection = getDBConnection();
			
			for(int i=0; i<teamList.size(); i++)
			{
				insertTableSQL = "INSERT INTO TeamTable VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
				
				preparedStatement = dbConnection.prepareStatement(insertTableSQL);

				preparedStatement.setString(1, teamList.get(i).getTeamID());
				preparedStatement.setString(2, teamList.get(i).getName());
				preparedStatement.setString(3, dateFormat.format(teamList.get(i).getOpeningDate()));
				preparedStatement.setString(4, teamList.get(i).getMainLocation());
				preparedStatement.setString(5, teamList.get(i).getGender());
				preparedStatement.setString(6, teamList.get(i).getDisability());
				preparedStatement.setInt(7, teamList.get(i).getGameNumber());
				preparedStatement.setInt(8, teamList.get(i).getWinNumber());
				preparedStatement.setInt(9, teamList.get(i).getAvgAge());
				preparedStatement.setInt(10, teamList.get(i).getGrade());
				preparedStatement.setInt(11, teamList.get(i).getAvgHeight());
				preparedStatement.setInt(12, teamList.get(i).getAvgWeight());
				preparedStatement.setInt(13, teamList.get(i).getMemberNumber());
				
				// execute insert SQL statement
				preparedStatement.executeUpdate();
			}

			for(int i=0; i<memberList.size(); i++)
			{
				insertTableSQL = "INSERT INTO MemberTable VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
				
				preparedStatement = dbConnection.prepareStatement(insertTableSQL);

				preparedStatement.setString(1, memberList.get(i).getId());
				preparedStatement.setString(2, memberList.get(i).getPw());
				preparedStatement.setString(3, memberList.get(i).getTeamID());
				preparedStatement.setString(4, memberList.get(i).getSportsID());
				preparedStatement.setString(5, memberList.get(i).getName());
				preparedStatement.setString(6, memberList.get(i).getGender());
				preparedStatement.setString(7, dateFormat.format(memberList.get(i).getBirthDate()));
				preparedStatement.setString(8, memberList.get(i).getPhone());
				preparedStatement.setString(9, memberList.get(i).getEmail());
				preparedStatement.setString(10, memberList.get(i).getLocation());
				preparedStatement.setInt(11, memberList.get(i).getHeight());
				preparedStatement.setInt(12, memberList.get(i).getWeight());
				preparedStatement.setString(13, memberList.get(i).getDisability());
				
				preparedStatement.executeUpdate();
			}

			System.out.println("Record is inserted into TeamTable table!");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static Connection getDBConnection() {
		Connection dbConnection = null;
		
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}

		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		return dbConnection;
	}
}