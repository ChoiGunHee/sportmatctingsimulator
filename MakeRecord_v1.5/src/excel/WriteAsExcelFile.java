package excel;

import java.awt.Label;
import java.io.File;
import java.util.ArrayList;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import table.Member;
import table.Match;
import table.Member;
import table.Stadium;
import table.Team;

public class WriteAsExcelFile {

	public void writeTeamList(ArrayList<Team> teamList)
	{
		WritableWorkbook workbook = null;
		
		WritableSheet sheet = null;
		
		jxl.write.Label label = null;
		
		String path = getClass().getResource("").getPath();
		File file = new File(path + "teamList.xls");
		
		try {
			workbook = Workbook.createWorkbook(file);
			
			workbook.createSheet("sheet1", 0);
			sheet = workbook.getSheet(0);
			
			label = new jxl.write.Label(0, 0, "Team ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(1, 0, "Name");
			sheet.addCell(label);
			
			label = new jxl.write.Label(2, 0, "Opening Date");
			sheet.addCell(label);
			
			label = new jxl.write.Label(3, 0, "Main Location");
			sheet.addCell(label);
			
			label = new jxl.write.Label(4, 0, "Gender");
			sheet.addCell(label);
			
			label = new jxl.write.Label(5, 0, "Disability");
			sheet.addCell(label);
			
			label = new jxl.write.Label(6, 0, "Game Number");
			sheet.addCell(label);
			
			label = new jxl.write.Label(7, 0, "Win Number");
			sheet.addCell(label);
			
			label = new jxl.write.Label(8, 0, "Average Age");
			sheet.addCell(label);
			
			label = new jxl.write.Label(9, 0, "Grade");
			sheet.addCell(label);
			
			label = new jxl.write.Label(10, 0, "Average Height");
			sheet.addCell(label);
			
			label = new jxl.write.Label(11, 0, "Average Weight");
			sheet.addCell(label);
			
			label = new jxl.write.Label(12, 0, "Member Number");
			sheet.addCell(label);
			
			for(int i=0; i<teamList.size(); i++)
			{
				label = new jxl.write.Label(0, i+1, teamList.get(i).getTeamID());
				sheet.addCell(label);
				label = new jxl.write.Label(1, i+1, teamList.get(i).getName());
				sheet.addCell(label);
				label = new jxl.write.Label(2, i+1, teamList.get(i).getOpeningDate().toString());
				sheet.addCell(label);
				label = new jxl.write.Label(3, i+1, teamList.get(i).getMainLocation());
				sheet.addCell(label);
				label = new jxl.write.Label(4, i+1, teamList.get(i).getGender());
				sheet.addCell(label);
				label = new jxl.write.Label(5, i+1, teamList.get(i).getDisability());
				sheet.addCell(label);
				label = new jxl.write.Label(6, i+1, teamList.get(i).getGameNumber() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(7, i+1, teamList.get(i).getWinNumber() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(8, i+1, teamList.get(i).getAvgAge() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(9, i+1, teamList.get(i).getGrade() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(10, i+1, teamList.get(i).getAvgHeight() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(11, i+1, teamList.get(i).getAvgWeight() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(12, i+1, teamList.get(i).getMemberNumber() + "");
				sheet.addCell(label);
			}
			
			workbook.write();
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void writeMemberList(ArrayList<Member> memberList)
	{
		WritableWorkbook workbook = null;
		
		WritableSheet sheet = null;
		
		jxl.write.Label label = null;
		
		String path = getClass().getResource("").getPath();
		File file = new File(path + "memberList.xls");
		
		try {
			workbook = Workbook.createWorkbook(file);
			
			workbook.createSheet("sheet1", 0);
			sheet = workbook.getSheet(0);
			
			label = new jxl.write.Label(0, 0, "ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(1, 0, "Password");
			sheet.addCell(label);
			
			label = new jxl.write.Label(2, 0, "Team ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(3, 0, "Sports ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(4, 0, "Name");
			sheet.addCell(label);
			
			label = new jxl.write.Label(5, 0, "Gender");
			sheet.addCell(label);
			
			label = new jxl.write.Label(6, 0, "Birth Date");
			sheet.addCell(label);
			
			label = new jxl.write.Label(7, 0, "Phone Number");
			sheet.addCell(label);
			
			label = new jxl.write.Label(8, 0, "E-Mail");
			sheet.addCell(label);
			
			label = new jxl.write.Label(9, 0, "Location");
			sheet.addCell(label);
			
			label = new jxl.write.Label(10, 0, "Height");
			sheet.addCell(label);
			
			label = new jxl.write.Label(11, 0, "Weight");
			sheet.addCell(label);
			
			label = new jxl.write.Label(12, 0, "Disability");
			sheet.addCell(label);
			
			for(int i=0; i<memberList.size(); i++)
			{
				label = new jxl.write.Label(0, i+1, memberList.get(i).getId());
				sheet.addCell(label);
				label = new jxl.write.Label(1, i+1, memberList.get(i).getPw());
				sheet.addCell(label);
				label = new jxl.write.Label(2, i+1, memberList.get(i).getTeamID());
				sheet.addCell(label);
				label = new jxl.write.Label(3, i+1, memberList.get(i).getSportsID());
				sheet.addCell(label);
				label = new jxl.write.Label(4, i+1, memberList.get(i).getName());
				sheet.addCell(label);
				label = new jxl.write.Label(5, i+1, memberList.get(i).getGender());
				sheet.addCell(label);
				label = new jxl.write.Label(6, i+1, memberList.get(i).getBirthDate().toString());
				sheet.addCell(label);
				label = new jxl.write.Label(7, i+1, memberList.get(i).getPhone());
				sheet.addCell(label);
				label = new jxl.write.Label(8, i+1, memberList.get(i).getEmail());
				sheet.addCell(label);
				label = new jxl.write.Label(9, i+1, memberList.get(i).getLocation());
				sheet.addCell(label);
				label = new jxl.write.Label(10, i+1, memberList.get(i).getHeight() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(11, i+1, memberList.get(i).getWeight() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(12, i+1, memberList.get(i).getDisability());
				sheet.addCell(label);
			}
			
			workbook.write();
			workbook.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void writeMatchList(ArrayList<Match> matchList)
	{
		WritableWorkbook workbook = null;
		
		WritableSheet sheet = null;
		
		jxl.write.Label label = null;
		
		String path = getClass().getResource("").getPath();
		File file = new File(path + "matchList.xls");
		try {
			workbook = Workbook.createWorkbook(file);
			
			workbook.createSheet("sheet1", 0);
			sheet = workbook.getSheet(0);
			
			label = new jxl.write.Label(0, 0, "Match ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(1, 0, "Match Date");
			sheet.addCell(label);
			
			label = new jxl.write.Label(2, 0, "Team A");
			sheet.addCell(label);
			
			label = new jxl.write.Label(3, 0, "Team B");
			sheet.addCell(label);
			
			label = new jxl.write.Label(4, 0, "Winning Team");
			sheet.addCell(label);
			
			label = new jxl.write.Label(5, 0, "Team A Score");
			sheet.addCell(label);
			
			label = new jxl.write.Label(6, 0, "Team B Score");
			sheet.addCell(label);
			
			label = new jxl.write.Label(7, 0, "Stadium ID");
			sheet.addCell(label);
			
			for(int i=0; i<matchList.size(); i++)
			{
				label = new jxl.write.Label(0, i+1, matchList.get(i).getMatchID());
				sheet.addCell(label);
				label = new jxl.write.Label(1, i+1, matchList.get(i).getMatchDate().toString());
				sheet.addCell(label);
				label = new jxl.write.Label(2, i+1, matchList.get(i).getTeamA());
				sheet.addCell(label);
				label = new jxl.write.Label(3, i+1, matchList.get(i).getTeamB());
				sheet.addCell(label);
				label = new jxl.write.Label(4, i+1, matchList.get(i).getWinningTeam());
				sheet.addCell(label);
				label = new jxl.write.Label(5, i+1, matchList.get(i).getTeamAScore() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(6, i+1, matchList.get(i).getTeamBScore() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(7, i+1, matchList.get(i).getStadiumID());
				sheet.addCell(label);
			}
			
			workbook.write();
			workbook.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void writeStadiumList(ArrayList<Stadium> stadiumList)
	{
		WritableWorkbook workbook = null;
		
		WritableSheet sheet = null;
		
		jxl.write.Label label = null;
		
		String path = getClass().getResource("").getPath();
		File file = new File(path + "stadiumList.xls");
		try {
			workbook = Workbook.createWorkbook(file);
			
			workbook.createSheet("sheet1", 0);
			sheet = workbook.getSheet(0);
			
			label = new jxl.write.Label(0, 0, "Stadium ID");
			sheet.addCell(label);
			
			label = new jxl.write.Label(1, 0, "Coordinate X");
			sheet.addCell(label);
			
			label = new jxl.write.Label(2, 0, "Coordinate Y");
			sheet.addCell(label);
			
			for(int i=0; i<stadiumList.size(); i++)
			{
				label = new jxl.write.Label(0, i+1, stadiumList.get(i).getStadiumID());
				sheet.addCell(label);
				label = new jxl.write.Label(1, i+1, stadiumList.get(i).getCoordinateX() + "");
				sheet.addCell(label);
				label = new jxl.write.Label(2, i+1, stadiumList.get(i).getCoordinateY() + "");
				sheet.addCell(label);
			}
			
			workbook.write();
			workbook.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
