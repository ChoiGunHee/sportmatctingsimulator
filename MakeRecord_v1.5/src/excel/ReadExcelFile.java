package excel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadExcelFile {
	
	public ArrayList<String> readFile(String filename)
	{
		ArrayList<String> stadiumInfo = new ArrayList<String>();
		
		Workbook wb = null;
		String path = getClass().getResource("").getPath();
		File file = new File(path + filename);
		
		try {
			wb = Workbook.getWorkbook(file);
			Sheet s = wb.getSheet(0);
			int row = s.getRows();
			int column = s.getColumns();
			
			for(int i=0; i<row; i++)
			{
				for(int j=0; j<column; j++)
				{
					Cell c = s.getCell(j, i);
					stadiumInfo.add(c.getContents());
				}
			}
		} catch (BiffException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return stadiumInfo;
	}
}
