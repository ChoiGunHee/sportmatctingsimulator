package record;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import table.Match;

public class MatchRecords {

	static ArrayList<Match> matchList;
	
	public MatchRecords(){
		
		matchList = new ArrayList<Match>();
		
		TeamRecords teamRecords = null;
		int teamNumber = teamRecords.teamList.size();
		
		/* 각 팀 당 멤버 수 */
		int[] memberNum = new int[teamNumber];
		for(int i=0; i<teamNumber; i++)
			memberNum[i] = teamRecords.teamList.get(i).getMemberNumber();
		
		for(int i=0; i<teamNumber; i++){
			String TeamID = teamRecords.teamList.get(i).getTeamID();
			String teamA, teamB = null, winningTeam = null;
			int teamAScore = 0, teamBScore = 0;
			int year = 2016, month, day;
			int memberSum=0, memberID=0;
			int teamA_gameNumber=0;
			Date date = null;
			
			Random random = new Random();
			
			teamA = TeamID;
			teamA_gameNumber = teamRecords.teamList.get(i).getGameNumber();

			/* teamB  */
			for(int j=0; j<teamNumber; j++){
				if(i == j) continue;
				
				Match match;
				Double rating;
				int[] selectedMember;
				
				DecimalFormat doubleFormat = new DecimalFormat("#.#");
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				
				/* teamB ID, 상대팀 결정  */
				TeamID = teamRecords.teamList.get(j).getTeamID();
				teamB = TeamID;
				
				/* 경기 날짜 */
				month = random.nextInt(12) + 1; //1~12월
				day = randomDay(month);
				try {
					date = dateFormat.parse(year + "-" + month + "-" + day);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/* 경기 결과 */
				teamAScore = random.nextInt(6); //0~5
				teamBScore = random.nextInt(6); //0~5
				
				/* 이긴 팀 */
				if(teamAScore == teamBScore) 
					winningTeam = null;
				else if(teamAScore > teamBScore)
					winningTeam = teamA;
				else if(teamAScore < teamBScore)
					winningTeam = teamB;
				
				/* (TeamB) TeamB 내 멤버 중 경기에 참여할 11명을 랜덤으로 선출 */
				selectedMember = selectMember(memberNum[j], 11);
				
				/* (TeamB) 개인 ID, 평점 정보  */
				for(int k=0; k<j; k++)
					memberSum += memberNum[k];
				memberID = memberSum;

				/* 종목 정보 테이블의 충성도 갱신 */
				TeamRecords teamRecords1 = null;
				int teamB_gameNumber=0;
				
				for(int x=0; x<teamNumber; x++)
					if(teamRecords1.teamList.get(x).getTeamID().equals(teamB))
						teamB_gameNumber = teamRecords1.teamList.get(x).getGameNumber();

				
				/* (TeamB) 멤버 별 경기기록  입력  */
				for(int k=0; k<memberNum[j]+1; k++){
					if(selectedMember[k] == 1){
						rating = random.nextDouble() * 10;
						
						match = new Match(memberID+k, teamB, date, teamA, teamB, null,
								teamAScore, teamBScore, winningTeam, Double.parseDouble(doubleFormat.format(rating)));
						matchList.add(match);
						
						new SportsRecords(memberID+k, teamB, teamB_gameNumber);
					}
				}
				memberSum = 0;

				/* (teamA) teamA 내 멤버 중 경기에 참여할 11명을 랜덤으로 선출 */
				selectedMember = selectMember(memberNum[i], 11);
				
				/* teamA 멤버 별 경기기록  입력  */
				for(int k=0; k<i; k++)
					memberSum += memberNum[k];
				memberID = memberSum;
				
				for(int k=0; k<memberNum[i]+1; k++){
					if(selectedMember[k] == 1){
						rating = random.nextDouble() * 10;
	
						match = new Match(memberID+k, teamA, date, teamA, teamB, null,
								teamAScore, teamBScore, winningTeam, Double.parseDouble(doubleFormat.format(rating)));
						matchList.add(match);
						
						new SportsRecords(memberID+k, teamA, teamA_gameNumber);
					}
				}
				memberSum = 0;
			}
		}
		
		print();

	}
	
	/* 랜덤으로 경기에 참여할 멤버를 뽑는 함수 (인자 : 멤버수, 뽑을 멤버 수)*/
	public int[] selectMember(int memberNum, int selectNum){
		int[] arr = new int[memberNum+1];
		Random random = new Random();
		int i, member;
		
		for(i=0; i<selectNum; i++)
			arr[i] = 0;
		
		for(i=0; i<selectNum; i++){
			member = random.nextInt(memberNum) + 1;
			
			while(member == 0 || arr[member] == 1)
				member = random.nextInt(memberNum) + 1;
			arr[member] = 1;
		}
		
		//선택된 멤버를 나타낸 배열
		return arr; 
	}

	public String formatNumber4(int i){
		return String.format("%04d", i);
	}
	
	public int randomDay(int month){
		int day; 
		Random random = new Random();
		
		switch(month){
		case 1:	case 3:	case 5:
		case 7:	case 8:	case 10: case 12:
			day = random.nextInt(31) + 1;
			break;
		case 2:
			day = random.nextInt(28) + 1;
			break;
		case 4:	case 6:	case 9:	case 11:
			day = random.nextInt(30) + 1;
			break;
			default:
				day = 0;
				break;
		}
		return day;
	}
	
	public void print(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		/* 출력  */
		/* Match Table */
		System.out.println("-------------------Match 데이터---------------------");
		System.out.println(matchList.size());
		for(int i=0; i<matchList.size(); i++){
			System.out.print(matchList.get(i).getMemberID() + " ");
			System.out.print(matchList.get(i).getTeamID() + " ");
//			System.out.print(dateFormat.format(matchList.get(i).getMatchDate()) + " ");
//			System.out.print(matchList.get(i).getTeamA() + " ");
//			System.out.print(matchList.get(i).getTeamB() + " ");
//			System.out.print(matchList.get(i).getStadiumID() + " ");
//			System.out.print(matchList.get(i).getTeamAScore() + " ");
//			System.out.print(matchList.get(i).getTeamBScore() + " ");
//			System.out.print(matchList.get(i).getWinningTeam() + " ");
			System.out.println(matchList.get(i).getRating() + " ");
		}
	}
}
