package record;

import java.util.ArrayList;

import table.Grade;

public class GradeRecords {
	
	ArrayList<Grade> gradeList;
	Grade grade1, grade2;
	
	public GradeRecords(){
		
		//클래스 Grade 배열
		gradeList = new ArrayList<Grade>();
		
		grade1 = new Grade("manager", "관리자");
		grade2 = new Grade("regular", "정회원");
	
		gradeList.add(grade1);
		gradeList.add(grade2);
		
//		print();
	}
	
	public void print(){
		/* 출력  */
		System.out.println("-------------------Grade 데이터---------------------");
		System.out.println(gradeList.size());
		System.out.println(grade1.getGradeID() + " " + grade1.getGradeName());
		System.out.println(grade2.getGradeID() + " " + grade2.getGradeName());
		
	}
}
