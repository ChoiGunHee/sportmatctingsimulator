package record;

import java.util.ArrayList;

import excel.ReadExcelFile;
import excel.WriteAsExcelFile;
import table.Stadium;

public class StadiumRecords {

	public StadiumRecords(){

		ArrayList<Stadium> stadiumList = new ArrayList<Stadium>();

		// 경기장 리스트 생성
		ArrayList<String> excelList = new ArrayList<String>();
		ReadExcelFile readExcelFile = new ReadExcelFile();
		excelList = readExcelFile.readFile("stadium.xls");
		Stadium stadium = null;
		
		for(int i=0; i<excelList.size(); i++)
		{
			stadium = new Stadium(excelList.get(i), Double.parseDouble(excelList.get(++i)), Double.parseDouble(excelList.get(++i)));
			stadiumList.add(stadium);
		}

		WriteAsExcelFile.writeStadiumList(stadiumList);
	}
}
