package record;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import table.Member;
import table.Sports;
import table.Team;

public class TeamRecords {

	static ArrayList<Team> teamList;
	ArrayList<Member> memberList;
	static ArrayList<Sports> sportsList;
	int userNum = 1;
	
	public TeamRecords(){

		teamList = new ArrayList<Team>();
		memberList = new ArrayList<Member>();
		sportsList = new ArrayList<Sports>();
		
		String teamID;
		
		for(int i=1; i<=12; i++){
			try {
				Team team = new Team(i);
				teamID = team.getTeamID();
				
				Random random = new Random();
				DecimalFormat doubleFormat = new DecimalFormat("#.#");
				
				int memberNum, ageSum=0;
				int[] memberAges = null;
				int[] memberMan = null, memberWoman = null;
				int[] memberDisability = null;
				double[] memberHeights = null, memberWeights = null;
				double heightSum=0, weightSum=0;
				double avgHeight=0, avgWeight=0; 
				double avgAge=0;
							
				//팀 내 멤버 생성
				memberNum = random.nextInt(8) + 11; //11~18명의 멤버
				for(int j=1; j<=memberNum; j++){
					int currentYear = new Date().getYear();
					int age;
					
					memberAges = new int[memberNum];
					memberMan = new int[memberNum];
					memberWoman = new int[memberNum];
					memberDisability = new int[memberNum];
					memberHeights = new double[memberNum];
					memberWeights = new double[memberNum];
					
					Member member = new Member(j, userNum++, teamID, genderToString(team.getManRate(), team.getWomanRate()),
							team.getAvgAge(), team.getMainStadium(), disabilityToString(team.getDisabilityRate()));
					memberList.add(member);
					
					age = currentYear - member.getBirthDate().getYear();
					ageSum += age;
					heightSum += member.getHeight();
					weightSum += member.getWeight();
					
					memberAges[j-1] = age;
					memberHeights[j-1] = member.getHeight(); 
					memberWeights[j-1] = member.getWeight();
					if(member.getGender().equals("M")){
						memberMan[j-1] = 1;
						memberWoman[j-1] = 0;
					}
					else if(member.getGender().equals("F")){
						memberMan[j-1] = 0;
						memberWoman[j-1] = 1;
					}
					if(member.getDisability().equals("Y"))
						memberDisability[j-1] = 1;
					else
						memberDisability[j-1] = 0;
					
					/* 종목 테이블 데이터 생성 */
					Sports sport = new Sports(userNum-1, teamID, 0);
					sportsList.add(sport);
				}
				
				avgAge = (double) ageSum / memberNum;
				avgHeight = heightSum / (double) memberNum;
				avgWeight = weightSum / (double) memberNum;
				
				team.setAvgAge(Double.parseDouble(doubleFormat.format(avgAge)));
				team.setAvgHeight(Double.parseDouble(doubleFormat.format(avgHeight)));
				team.setAvgWeight(Double.parseDouble(doubleFormat.format(avgWeight)));
				team.setMemberNumber(memberNum);
				team.setAvgAge_SD(calculateSD_int(memberNum, memberAges, avgAge));
				team.setAvgHeight_SD(calculateSD_double(memberNum, memberHeights, avgHeight));
				team.setAvgweight_SD(calculateSD_double(memberNum, memberWeights, avgWeight));
				team.setManRate_SD(calculateSD_rate(memberNum, memberMan, team.getManRate()));
				team.setWomanRate_SD(calculateSD_rate(memberNum, memberWoman, team.getWomanRate()));
				team.setDisability_SD(calculateSD_rate(memberNum, memberDisability, team.getDisabilityRate()));
				
				teamList.add(team);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		print_team();
		print_member();
//		print_sports();
	}
	
	public double calculateSD_rate(int memberNum, int[] arr, double rate){
		double sum=0;
		String SD=null;
		Math math = null;
		
		DecimalFormat doubleFormat = new DecimalFormat("#.#");

		for(int i=0; i<memberNum; i++)
			sum += (arr[i] - rate) * (arr[i] - rate);
		
		SD = doubleFormat.format(math.sqrt((double)sum/(memberNum-1)));
		return Double.parseDouble(SD);
	}

	public double calculateSD_int(int memberNum, int[] arr, double avgValue){
		int sum=0;
		String SD=null;
		Math math = null;
		
		DecimalFormat doubleFormat = new DecimalFormat("#.#");

		for(int i=0; i<memberNum; i++)
			sum += (arr[i] - avgValue) * (arr[i] - avgValue);
		
		SD = doubleFormat.format(math.sqrt((double)sum/(memberNum-1)));
		return Double.parseDouble(SD);
	}
	
	public double calculateSD_double(int memberNum, double[] arr, double avgValue){
		double sum=0;
		String SD=null;
		Math math = null;
		
		DecimalFormat doubleFormat = new DecimalFormat("#.#");

		for(int i=0; i<memberNum; i++)
			sum += (arr[i] - avgValue) * (arr[i] - avgValue);
		
		SD = doubleFormat.format(math.sqrt(sum/(memberNum-1)));
		return Double.parseDouble(SD);
	}
	
	public String genderToString(double manRate, double womanRate){
		if(manRate == 0)
			return "F";
		else if(womanRate == 0)
			return "M";
		
		return "B";  //Both(남자,여자 섞여있는 팀)
	}
	
	public String disabilityToString(double disabilityRate){
		if(disabilityRate == 0)
			return "N";
		return "Y";
	}
	
	public void print_team(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		/* Team Table */
		System.out.println("-------------------Team 데이터---------------------");
		System.out.println(teamList.size());
		System.out.println("팀ID	팀명	개설일	주경기장	남자비율	비율표준편차	여자비율"
				+ "	비율표준편차	장애비율	비율표준편차	게임수	이긴수	평균나이"
				+ "	나이표준편차	팀평점	평균신장	표준편차"
				+ "	평균체중	표준편차	멤버수");
		for(int i=0; i<teamList.size(); i++){
			System.out.print(teamList.get(i).getTeamID() + " ");
			System.out.print(teamList.get(i).getTeamName() + " ");
			System.out.print(dateFormat.format(teamList.get(i).getOpeningDate()) + " ");
			System.out.print(teamList.get(i).getMainStadium() + " ");
			System.out.print(teamList.get(i).getManRate() + " ");
			System.out.print(teamList.get(i).getManRate_SD() + " ");
			System.out.print(teamList.get(i).getWomanRate() + " ");
			System.out.print(teamList.get(i).getWomanRate_SD() + " ");
			System.out.print(teamList.get(i).getDisabilityRate() + " ");
			System.out.print(teamList.get(i).getDisability_SD() + " ");
			System.out.print(teamList.get(i).getGameNumber() + " ");
			System.out.print(teamList.get(i).getWinNumber() + " ");
			System.out.print(teamList.get(i).getAvgAge() + " ");
			System.out.print(teamList.get(i).getAvgAge_SD() + " ");
			System.out.print(teamList.get(i).getRating() + " ");
			System.out.print(teamList.get(i).getAvgHeight() + " ");
			System.out.print(teamList.get(i).getAvgHeight_SD() + " ");
			System.out.print(teamList.get(i).getAvgWeight() + " ");
			System.out.print(teamList.get(i).getAvgweight_SD() + " ");
			System.out.println(teamList.get(i).getMemberNumber());
		}
	}
	
	public void print_member(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		/* Member Table */
		System.out.println("-------------------Member 데이터-------------------");
		System.out.println(memberList.size());
		System.out.println("멤버ID	멤버PW	종목ID	이름	성별"
				+ "	생년월일	핸드폰		이메일		위치	신장	체중	장애여부");
		for(int i=0; i<memberList.size(); i++){
			System.out.print(memberList.get(i).getMemberID()+" ");
			System.out.print(memberList.get(i).getMemberPW()+" ");
			System.out.print(memberList.get(i).getSportsID()+" ");
			System.out.print(memberList.get(i).getName()+" ");
			System.out.print(memberList.get(i).getGender()+" ");
			System.out.print(dateFormat.format(memberList.get(i).getBirthDate())+" ");
			System.out.print(memberList.get(i).getPhone()+" ");
			System.out.print(memberList.get(i).getEmail()+" ");
			System.out.print(memberList.get(i).getLocation()+" ");
			System.out.print(memberList.get(i).getHeight()+" ");
			System.out.print(memberList.get(i).getWeight()+" ");
			System.out.println(memberList.get(i).getDisability());
		}
	}
	
	public void print_sports(){
		
		/* Sports Table */
		System.out.println("-------------------Sports 데이터-------------------");
		System.out.println(sportsList.size());
		System.out.println("종목ID		팀ID			팀충성도");
		for(int i=0; i<sportsList.size(); i++){
			System.out.print(sportsList.get(i).getSportsID() + " ");
			System.out.print(sportsList.get(i).getTeamID() + " ");
			System.out.println(sportsList.get(i).getTeamLoyalty());
		}
	}
}
