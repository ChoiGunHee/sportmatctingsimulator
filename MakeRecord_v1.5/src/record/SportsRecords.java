package record;

import java.text.DecimalFormat;
import java.util.ArrayList;

import table.Match;
import table.MemberGrade;
import table.Sports;
import table.Team;

public class SportsRecords {

	ArrayList<Sports> sportsList;
	
	/* Match 테이블에 따라 업데이트됨. */
	public SportsRecords(int memberID, String teamID, int team_gameNumber){
		
		TeamRecords teamRecords = null;
		double loyality;
		
		sportsList = teamRecords.sportsList; //갱신해야할 리스트

		/* 갱신 위해 종목 정보 레코드 찾기 */
		for(int i=0; i<sportsList.size(); i++){
			
			if(sportsList.get(i).getSportsID().equals(sportsID(memberID))
					&& sportsList.get(i).getTeamID().equals(teamID)){
				
				int member_gameNumber;
				DecimalFormat doubleFormat = new DecimalFormat("#.#");
				
				/* 갱신(충성도 없데이트) */
				sportsList.get(i).setGameNumber(sportsList.get(i).getGameNumber()+1);
				member_gameNumber = sportsList.get(i).getGameNumber();
				loyality = Double.parseDouble(doubleFormat.format( (double) member_gameNumber / team_gameNumber));

				sportsList.get(i).setTeamLoyalty(loyality);
				
//				print_index(i);
				break;
			}
		}
		
	}
	
	public String sportsID(int num){
		return "Sports_PID" + formatNumber4(num);
	}
	
	public String formatNumber4(int i){
		return String.format("%04d", i);
	}

	public void print_index(int i){
		System.out.println("===Sports Records===");
			System.out.print(sportsList.get(i).getSportsID() + " ");
			System.out.print(sportsList.get(i).getTeamID() + " ");
			System.out.print(sportsList.get(i).getTeamLoyalty() + " ");
			System.out.println();
	}
	
	public void print(){
		System.out.println("===Sports Records===");
		System.out.println(sportsList.size());
		for(int i=0; i<sportsList.size(); i++){
			System.out.print(sportsList.get(i).getSportsID() + " ");
			System.out.print(sportsList.get(i).getTeamID() + " ");
			System.out.print(sportsList.get(i).getTeamLoyalty() + " ");
			System.out.println();
		}
	}
}
