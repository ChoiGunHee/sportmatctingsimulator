package database;

import java.io.File;
import java.util.ArrayList;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class DataClass {
	
	public ArrayList<String> getDataList(String file_name){
        ArrayList<String> dataList = new ArrayList<String>();

        try {
        	//InputStream is = getClass().getResource("").getPath("words.xls");
        	String path = getClass().getResource("").getPath();
        	//System.out.println(path);
        	File file = new File(path + file_name);
            Workbook wb = Workbook.getWorkbook(file);
            Sheet s = wb.getSheet(0);
            int row = 329; //s.getRows();
            int col = 1; //s.getColumns();

            for(int i=0; i<row; i++){
                for(int j=0; j<col; j++){
                    Cell c = s.getCell(j, i);
                    dataList.add(c.getContents());
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return dataList;
    }
/*
	public static void main(String[] args) {

		DataClass dataClass = new DataClass();
		ArrayList<String> teamList = dataClass.getDataList("namelist.xls");
		
		System.out.println(teamList.size());
		System.out.println(teamList);
	}
*/	
}
