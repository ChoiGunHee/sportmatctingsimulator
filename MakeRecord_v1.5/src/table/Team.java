package table;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/*
 * (변경)
 * String gender -> double manRate, womanRate
 * String disability -> double disabilityRate
 * int avgAge -> double avgAge;
 * int grade -> double rating;
 * int avgHeight -> double avgHeight;
 * int avgWeight -> double avgWeight;
 * (추가)
 * double avgAge_SD; //나이 표준 편차
 * double manRate_SD; //성별 표준 편차
 * double womanRate_SD;
 * double disability_SD; //장애 표준 편차
 * double avgHeight_SD; //신장 표준 편차
 * double avgweight_SD; //체중 표준 편차
 */

public class Team {
	
	private String teamID;
	private String teamName;
	private Date openingDate;
	private	String mainStadium;
	private	double manRate; //남자 성별 비율
	private double manRate_SD;
	private	double womanRate; //여자 성별 비율
	private double womanRate_SD;
	private double disabilityRate;
	private double disability_SD;
	private int gameNumber;
	private int winNumber;
	private double avgAge;
	private double avgAge_SD; //나이 표준 편차
	private double rating; //팀의 평점 변수(일단 제외)
	private double avgHeight; //팀의 평균 신장
	private double avgHeight_SD; //신장 표준 편차
	private double avgWeight; //팀의 평균 체중
	private double avgweight_SD; //체중 표준 편차
	private int memberNumber;
	
	public Team(int num) throws ParseException{
		Random random = new Random();
		
		teamID = "TID" + formatNumber4(num);
		teamName = "Team" + formatNumber4(num);
		openingDate = random_openingDate();
		mainStadium = (num<=6) ? "성남" : "서울";
		if(num <= 10){
			manRate = 1;
			womanRate = 0;
		}
		else{
			manRate = 0;
			womanRate = 1;
		}
		disabilityRate = (num==9 || num==10 || num==12) ? 1 : 0;
		gameNumber = random.nextInt(51); //0~50
		if(gameNumber == 0) winNumber = 0;
		else winNumber = random.nextInt(gameNumber);

		//팀의 구성이 20대:30대:40대:50대:60대 = 3:2:2:2:1의 비율로 만들어지도록
		//단순히 구현한 것.
		if(num<=3 || num==11) avgAge = 20; //팀의 평균나이  20대
		else if(num>=4 || num<=5) avgAge = 30;
		else if(num>=6 || num<=7) avgAge = 40;
		else if(num>=8 || num<=9) avgAge = 50;
		else if(num == 10) avgAge = 60;
		rating = 0;
		avgHeight = 0;
		avgWeight = 0;
		memberNumber = 0;
		
		
	}
	
//	public double calculateSD(double avgValue, int memberNum){
//		Math math;
//		double sum=0, avg=0;
//		
//		for(int i=0; i<memberNum; i++)
//			sum += 
//		
//	}
	
	public String formatNumber4(int i){
		return String.format("%04d", i);
	}
	
	Date random_openingDate() throws ParseException{
		int year, month, day; 
		Random random = new Random();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		year = random.nextInt(16) + 2000; //2000~2015 값 중 랜덤으로 year에 저장
		month = random.nextInt(12) + 1; //1~12월
		
		switch(month){
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = random.nextInt(31) + 1;
			break;
		case 2:
			day = random.nextInt(28) + 1;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = random.nextInt(30) + 1;
			break;
			default:
				day = 0;
				break;
		}
		
		return dateFormat.parse(year + "-" + month + "-" + day);
	}

	public String getTeamID() {
		return teamID;
	}

	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public String getMainStadium() {
		return mainStadium;
	}

	public void setMainStadium(String mainStadium) {
		this.mainStadium = mainStadium;
	}

	public double getManRate() {
		return manRate;
	}

	public void setManRate(double manRate) {
		this.manRate = manRate;
	}

	public double getManRate_SD() {
		return manRate_SD;
	}

	public void setManRate_SD(double manRate_SD) {
		this.manRate_SD = manRate_SD;
	}

	public double getWomanRate() {
		return womanRate;
	}

	public void setWomanRate(double womanRate) {
		this.womanRate = womanRate;
	}

	public double getWomanRate_SD() {
		return womanRate_SD;
	}

	public void setWomanRate_SD(double womanRate_SD) {
		this.womanRate_SD = womanRate_SD;
	}

	public double getDisabilityRate() {
		return disabilityRate;
	}

	public void setDisabilityRate(double disabilityRate) {
		this.disabilityRate = disabilityRate;
	}

	public double getDisability_SD() {
		return disability_SD;
	}

	public void setDisability_SD(double disability_SD) {
		this.disability_SD = disability_SD;
	}

	public int getGameNumber() {
		return gameNumber;
	}

	public void setGameNumber(int gameNumber) {
		this.gameNumber = gameNumber;
	}

	public int getWinNumber() {
		return winNumber;
	}

	public void setWinNumber(int winNumber) {
		this.winNumber = winNumber;
	}

	public double getAvgAge() {
		return avgAge;
	}

	public void setAvgAge(double avgAge) {
		this.avgAge = avgAge;
	}

	public double getAvgAge_SD() {
		return avgAge_SD;
	}

	public void setAvgAge_SD(double avgAge_SD) {
		this.avgAge_SD = avgAge_SD;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public double getAvgHeight() {
		return avgHeight;
	}

	public void setAvgHeight(double avgHeight) {
		this.avgHeight = avgHeight;
	}

	public double getAvgHeight_SD() {
		return avgHeight_SD;
	}

	public void setAvgHeight_SD(double avgHeight_SD) {
		this.avgHeight_SD = avgHeight_SD;
	}

	public double getAvgWeight() {
		return avgWeight;
	}

	public void setAvgWeight(double avgWeight) {
		this.avgWeight = avgWeight;
	}

	public double getAvgweight_SD() {
		return avgweight_SD;
	}

	public void setAvgweight_SD(double avgweight_SD) {
		this.avgweight_SD = avgweight_SD;
	}

	public int getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(int memberNumber) {
		this.memberNumber = memberNumber;
	}
	
}
