package table;

public class MemberGrade {
	
	private String memberID; 
	private String teamID;
	private String gradeID;

	public MemberGrade(int memberID, String teamID, String gradeID){
		this.memberID = "PID" + formatNumber4(memberID);;
		this.teamID = teamID;
		this.gradeID = gradeID;
	}
	
	public String formatNumber4(int i){
		return String.format("%04d", i);
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getTeamID() {
		return teamID;
	}

	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}

	public String getGradeID() {
		return gradeID;
	}

	public void setGradeID(String gradeID) {
		this.gradeID = gradeID;
	}
	
	
}
