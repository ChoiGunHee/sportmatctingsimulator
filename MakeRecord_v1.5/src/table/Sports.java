package table;

public class Sports {
	
	private String sportsID;
	private String teamID;
	private double teamLoyalty; //팀 충성도
	private int gameNumber; //경기수, DB에는 존재X

	public Sports(int num, String teamID, double teamLoyalty){
		this.sportsID = "Sports_PID" + formatNumber4(num);
		this.teamID = teamID;
		this.teamLoyalty = teamLoyalty;
		this.gameNumber = 0;
	}

	public String formatNumber4(int i){
		return String.format("%04d", i);
	}
	
	public String getSportsID() {
		return sportsID;
	}

	public void setSportsID(String sportsID) {
		this.sportsID = sportsID;
	}

	public String getTeamID() {
		return teamID;
	}

	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}

	public double getTeamLoyalty() {
		return teamLoyalty;
	}

	public void setTeamLoyalty(double teamLoyalty) {
		this.teamLoyalty = teamLoyalty;
	}

	public int getGameNumber() {
		return gameNumber;
	}

	public void setGameNumber(int gameNumber) {
		this.gameNumber = gameNumber;
	}
	
	
}
