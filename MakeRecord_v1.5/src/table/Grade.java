package table;

public class Grade {
	
	private String gradeID; //Grade ID
	private String gradeName; //Grade name
	
	public Grade(String gradeID, String gradeName){
		this.gradeID = gradeID;
		this.gradeName = gradeName;
	}

	public String getGradeID() {
		return gradeID;
	}

	public void setGradeID(String gradeID) {
		this.gradeID = gradeID;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	
}
