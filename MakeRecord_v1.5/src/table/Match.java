package table;

import java.util.Date;

/*
 * (추가)
 * String memberID;
 * String teamID;
 * double rating;
 * (삭제)
 * String matchID;
 * 
 */

public class Match {
	private String memberID; //개인 ID
	private String teamID; //팀 ID
	private Date matchDate;
	private String teamA;
	private String teamB;
	private String stadiumID; //경기장 ID
	private int teamAScore;
	private int teamBScore;
	private String winningTeam;
	private double rating; //상대팀에 대한 평점
	
	//생성자
	public Match(int memberID, String teamID, Date matchDate, String teamA, String teamB,
			String stadiumID, int teamAScore, int teamBScore, String winningTeam, double rating){
		this.memberID = "PID" + formatNumber4(memberID);
		this.teamID = teamID;
		this.matchDate = matchDate;
		this.teamA = teamA;
		this.teamB = teamB;
		this.stadiumID = stadiumID;
		this.teamAScore = teamAScore;
		this.teamBScore = teamBScore;
		this.winningTeam = winningTeam;
		this.rating = rating;
	}
	
	public String formatNumber4(int i){
		return String.format("%04d", i);
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getTeamID() {
		return teamID;
	}

	public void setTeamID(String teamID) {
		this.teamID = teamID;
	}

	public Date getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(Date matchDate) {
		this.matchDate = matchDate;
	}

	public String getTeamA() {
		return teamA;
	}

	public void setTeamA(String teamA) {
		this.teamA = teamA;
	}

	public String getTeamB() {
		return teamB;
	}

	public void setTeamB(String teamB) {
		this.teamB = teamB;
	}

	public String getStadiumID() {
		return stadiumID;
	}

	public void setStadiumID(String stadiumID) {
		this.stadiumID = stadiumID;
	}

	public int getTeamAScore() {
		return teamAScore;
	}

	public void setTeamAScore(int teamAScore) {
		this.teamAScore = teamAScore;
	}

	public int getTeamBScore() {
		return teamBScore;
	}

	public void setTeamBScore(int teamBScore) {
		this.teamBScore = teamBScore;
	}

	public String getWinningTeam() {
		return winningTeam;
	}

	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

}
